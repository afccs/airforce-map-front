import { MaterialReactTable, useMaterialReactTable } from "material-react-table";
import { useEffect, useState } from "react";
import BaseButton from "../components/button/BaseButton";
import { addItem, openDB, STORE_NAME } from "../common/hooks/useIndexedDB";
import Box from "@mui/material/Box";
import TextInput from "../components/form/TextInput";

//idb 라이브러리를 사용하지 않고 indexedDB를 사용하여 작성
const IndexedDBStudy = () => {

    const [db, setDB] = useState<IDBDatabase | null>(null);
    const [ data, setData ] = useState<Person[]>([]);
    const [ name, setName ] = useState('');
    const [ age, setAge ] = useState(0);
    
    useEffect(() => {
        const initializeDB = async () => {
            try {
                const database = await openDB();
                setDB(database);
                fetchItems();
            } catch (error) {
                console.error('Failed to open database:', error);
            }
        };
    
        initializeDB();

        }, [db]);

    const handleAddItem = async () => {
        if (db) {
            const id = await addItem(db, { name, age });
            console.log('Item added with id:', id);
            setName('');
            setAge(0);
            fetchItems();
        }
    };
    
    const fetchItems = async () => {
        if (db) {
            try {
                const transaction = db.transaction(STORE_NAME, 'readonly');
                const store = transaction.objectStore(STORE_NAME);
                const request = store.getAll();
        
                request.onsuccess = () => {
                    setData(request.result);
                };
        
                request.onerror = () => {
                console.error('Failed to fetch items:', request.error);
                };
            }catch(err: any){
                console.log('error', err)
            }
        }
    };
    
    const table = useMaterialReactTable({
        columns,
        data,
    });

    return (
        <Box sx={{padding:10}}>
            <Box sx={{display:'flex', justifyContent:'space-around', alignItems:'center'}}>
                    <TextInput sx={{width:200}} variant="outlined" value={name} onChange={(e) => setName(e.target.value)} />
                    <TextInput sx={{width:200}} type='number' variant="outlined" value={age} onChange={(e) => setAge(parseInt(e.target.value))} />
                    <BaseButton sx={{width:200}} title="추가" onClick={handleAddItem}/>
                </Box>
            <MaterialReactTable 
                table={table} 
            />
        </Box>
    )
}

export default IndexedDBStudy;

type Person = {
    name: string;
    age: number;
};

const columns = [
    {
        accessorKey: 'name',
        header: 'Name',
    },
    {
        accessorKey: 'age',
        header: 'Age',
    },
];
