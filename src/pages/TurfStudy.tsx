import React, { useEffect, useRef } from "react";
// import Map from "ol/Map";
// import View from "ol/View";
// import { Tile as TileLayer } from "ol/layer";
// import OSM from "ol/source/OSM";
// import VectorSource from "ol/source/Vector";
// import VectorLayer from "ol/layer/Vector";
// import { Feature } from "ol";
// import { LineString, Point, Polygon } from "ol/geom";
// import { Style, Stroke, Fill } from "ol/style";
// import { fromLonLat } from "ol/proj";
// import CircleStyle from "ol/style/Circle";
import * as turf from "@turf/turf";

function TurfStudy() {
  const mapElement = useRef<HTMLDivElement | null>(null);
//   const mapRef = useRef<Map | null>(null);

  useEffect(() => {
//     if (!mapElement.current) return;

//     // OpenLayers 맵 초기화
//     const map = new Map({
//       target: mapElement.current,
//       layers: [
//         new TileLayer({
//           source: new OSM(),
//         }),
//       ],
//       view: new View({
//         center: fromLonLat([0, 0]),
//         zoom: 2,
//       }),
//     });

    // Z 라인 (교차할 대상)
    const zLineCoordinates = [
      [-15, -10],
      [10, -30],
    ];
    // const zLine = new Feature({
    //   geometry: new LineString(zLineCoordinates.map(coord => fromLonLat(coord))),
    // });

    // A, B, C 라인 (교차 여부를 확인할 대상)
    const aLineCoordinates = [
      [-20, 0],
      [0, 20],
    ];
    const bLineCoordinates = [
      [-15, -15],
      [-5, -5],
    ];
    const cLineCoordinates = [
      [0, -20],
      [20, 0],
    ];

    // const aLine = new Feature({
    //   geometry: new LineString(aLineCoordinates.map(coord => fromLonLat(coord))),
    // });
    // const bLine = new Feature({
    //   geometry: new LineString(bLineCoordinates.map(coord => fromLonLat(coord))),
    // });
    // const cLine = new Feature({
    //   geometry: new LineString(cLineCoordinates.map(coord => fromLonLat(coord))),
    // });

    // // 스타일 설정
    // const createLineStyle = (color: string) =>
    //   new Style({
    //     stroke: new Stroke({
    //       color,
    //       width: 2,
    //     }),
    //   });

    // zLine.setStyle(createLineStyle("blue"));
    // aLine.setStyle(createLineStyle("red"));
    // bLine.setStyle(createLineStyle("green"));
    // cLine.setStyle(createLineStyle("orange"));

    // Turf.js를 사용하여 z 라인이 a, b, c 라인과 교차하는지 확인
    const zLineTurf = turf.lineString(zLineCoordinates);
    const aLineTurf = turf.lineString(aLineCoordinates);
    const bLineTurf = turf.lineString(bLineCoordinates);
    const cLineTurf = turf.lineString(cLineCoordinates);

    // 교차 여부 확인
    const checkIntersection = (line1: any, line2: any) => {
      const intersection = turf.lineIntersect(line1, line2);
      console.log(intersection);
      return intersection.features.length > 0;
    };

    if (checkIntersection(zLineTurf, aLineTurf)) {
      console.log("Blue 라인이 Red 라인과 교차합니다.");
    }
    if (checkIntersection(zLineTurf, bLineTurf)) {
      console.log("Blue 라인이 Green 라인과 교차합니다.");
    }
    if (checkIntersection(zLineTurf, cLineTurf)) {
      console.log("Blue 라인이 Orange 라인과 교차합니다.");
    }



    //폴리곤 내에 포인트 존재하는지 확인하는 코드
    // 폴리곤 좌표 (다각형)
    // const polygonCoordinates = [
    //   [
    //     [-5, -5],
    //     [5, -5],
    //     [5, 5],
    //     [-5, 5],
    //     [-5, -5],
    //   ],
    // ];

    // 폴리곤 피처
    // const polygon = new Feature({
    //   geometry: new Polygon(
    //     polygonCoordinates.map(ring => ring.map(coord => fromLonLat(coord)))
    //   ),
    // });

    // // 포인트 좌표 (확인할 지점)
    // const pointCoordinates = [2, 2];
    // const point = new Feature({
    //   geometry: new Point(fromLonLat(pointCoordinates)),
    // });

    // // 스타일 설정
    // polygon.setStyle(
    //   new Style({
    //     stroke: new Stroke({
    //       color: "green",
    //       width: 2,
    //     }),
    //     fill: new Fill({
    //       color: "rgba(0, 255, 0, 0.1)",
    //     }),
    //   })
    // );

    // point.setStyle(
    //   new Style({
    //     image: new CircleStyle({
    //       radius: 5,
    //       fill: new Fill({ color: "blue" }),
    //       stroke: new Stroke({
    //         color: "blue",
    //         width: 2,
    //       }),
    //     }),
    //   })
    // );
    
    // // 벡터 레이어 설정
    // const vectorSource = new VectorSource({
    //   features: [zLine, aLine, bLine, cLine, polygon, point],
    // });

    // const vectorLayer = new VectorLayer({
    //   source: vectorSource,
    // });

    // map.addLayer(vectorLayer);

    // Turf.js를 사용하여 포인트가 폴리곤 내부에 있는지 확인
    // const polygonTurf = turf.polygon(polygonCoordinates);
    // const pointTurf = turf.point(pointCoordinates);

    // const isInside = turf.booleanPointInPolygon(pointTurf, polygonTurf);

    // if (isInside) {
    //   console.log("포인트는 폴리곤 내부에 있습니다.");
    // } else {
    //   console.log("포인트는 폴리곤 외부에 있습니다.");
    // }

    // mapRef.current = map;

    return () => {
      mapElement.current = null;
    };
  }, []);

  return (
    <div style={{position:'absolute', width:'100%', height:'100%'}}>
      <div ref={mapElement} style={{ width: "100%", height: "400px" }} />
    </div>
  )
}

export default TurfStudy;
