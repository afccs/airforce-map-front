import { MaterialReactTable, useMaterialReactTable } from "material-react-table";
import { useEffect, useState } from "react";
import { addUser, getUsers } from "../common/hooks/useIDB";
import BaseButton from "../components/button/BaseButton";
import TextInput from "../components/form/TextInput";
import Box from "@mui/material/Box";

//idb 라이브러리를 사용하여 작성
const IDBStudy = () => {

    const [ data, setData ] = useState<Person[]>([]);
    const [ name, setName ] = useState('');
    const [ age, setAge ] = useState(0);

    useEffect(() => {
        fetchUser();
    }, [])

    const fetchUser = async () => {
        const users = await getUsers();
        setData(users);
    }

    const addData = async () => {
        await addUser({name, age});
        setName('');
        setAge(0);
        fetchUser();
    }

    const table = useMaterialReactTable({
        columns,
        data,
    });

    return (
        <Box sx={{padding:10}}>
            <Box sx={{display:'flex', justifyContent:'space-around', alignItems:'center'}}>
                <TextInput sx={{width:200}} variant="outlined" value={name} onChange={(e) => setName(e.target.value)} />
                <TextInput sx={{width:200}} type='number' variant="outlined" value={age} onChange={(e) => setAge(parseInt(e.target.value))} />
                <BaseButton sx={{width:200}} title="추가" onClick={addData}/>
            </Box>
            <MaterialReactTable 
                table={table} 
            />
        </Box>
    )
}

export default IDBStudy;

type Person = {
    id?: number;
    name: string;
    age: number
};

const columns = [
    {
        accessorKey: 'name',
        header: 'Name',
    },
    {
        accessorKey: 'age',
        header: 'Age',
    },
];
