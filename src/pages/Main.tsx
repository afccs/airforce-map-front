import { Box, Grid, Typography } from "@mui/material";
import useFullScreenStore from "../stores/useFullScreenStore";
import StatusBox from "../modules/widget/StatusBox";
import ShortReportStatusBox from "../components/widget/ShortReportStatusBox";
import TodayBox from "../components/widget/TodayBox";
import WeatherBox from "../components/widget/WeatherBox";
import DataTab from "../components/widget/DataTab";
import MaterialReactTableTest from "./MaterialReactTableTest";
import { useEffect, useState } from "react";
import BaseButton from "../components/button/BaseButton";
import { toastShow } from "../components/alert/ToastMessage";

/**
 * 메인 페이지 (프로토타입 샘플)
 * @returns JSX.Element(Page)
 */
const Main = () => {
	const { isFullScreenOpen } = useFullScreenStore();

	const { innerWidth } = window;

	//electron
	const [message, setMessage] = useState("");
	useEffect(() => {
		window?.bridge?.listenChannelMessage(setMessage);
	}, []);

	const handleClick = () => {
		window.bridge.sendMessage("send message by preload");
	};

	useEffect(() => {
		if(message === '') return
		toastShow({
			type: "info",
			title: "메세지도착",
			message,
		})
	}, [message])

	return (
		<div
			style={{
				padding: isFullScreenOpen === "f" ? "4% 3%" : "2% 3%",
				position: "absolute",
				top: isFullScreenOpen === "f" ? "0.8em" : "8.3em",
				left: isFullScreenOpen === "f" ? "0.8em" : "16em",
				width: isFullScreenOpen === "f" ? innerWidth : innerWidth - 200,
			}}
		>
			<Grid container spacing={2}>
				<Grid item xs={10}>
					<Box>
						<Typography>Electron 테스트 : {message}</Typography>
						<BaseButton onClick={handleClick} title="Electron" />
					</Box>
					<Box>
						<Typography>Material React Table 테스트</Typography>
						<MaterialReactTableTest />
					</Box>
					<Grid container spacing={2}>
						<Grid item xs={3}>
							<ShortReportStatusBox
								title="Box 1"
								color="yellow"
								subtitle="Message 1"
								mainNumber={30}
								suffix="명"
								percent={20}
							/>
						</Grid>
						<Grid item xs={3}>
							<ShortReportStatusBox
								title="Box 2"
								color="green"
								subtitle="Message 2"
								mainNumber={90}
								suffix="%"
								percent={90}
							/>
						</Grid>
						<Grid item xs={3}>
							<ShortReportStatusBox
								title="Box 3"
								color="orange"
								subtitle="Message 3"
								mainNumber={40}
								suffix="%"
								percent={40}
							/>
						</Grid>
						<Grid item xs={3}>
							<ShortReportStatusBox
								title="Box 4"
								color="green"
								subtitle="Message 4"
								mainNumber={20}
								suffix="명"
								percent={10}
							/>
						</Grid>
					</Grid>
					<Grid container spacing={2}>
						<Grid item xs={6}>
							<DataTab />
						</Grid>
					</Grid>
				</Grid>
				<Grid item xs={2}>
					<TodayBox />
					<StatusBox severity="danger" title="title 1" desc="왕왕 50%" />
					<StatusBox severity="normal" title="title 2" desc="crystal clear" />
					<StatusBox severity="warn" title="title 3" desc="35% Operation Rate " />
					<WeatherBox />
				</Grid>
			</Grid>
		</div>
	);
};

export default Main;
