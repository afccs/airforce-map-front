// AES 암호화, 복호화 구현
import React, { useState } from "react";
import CryptoJS from "crypto-js";

const CryptoStudy = () => {
  const [plainText, setPlainText] = useState("");
  const [encryptedText, setEncryptedText] = useState("");
  const [decryptedText, setDecryptedText] = useState("");
  const secretKey = "your-secret-key"; // 안전한 키로 교체해야 합니다.

  // 암호화 함수
  const encrypt = (text: string) => {
    const ciphertext = CryptoJS.AES.encrypt(text, secretKey).toString();
    setEncryptedText(ciphertext);
  };

  // 복호화 함수
  const decrypt = (ciphertext: string) => {
    const bytes = CryptoJS.AES.decrypt(ciphertext, secretKey);
    const originalText = bytes.toString(CryptoJS.enc.Utf8);
    setDecryptedText(originalText);
  };

  return (
    <div>
      <h1>AES 암호화 예제</h1>
      <div>
        <textarea
          value={plainText}
          onChange={(e) => setPlainText(e.target.value)}
          placeholder="암호화할 텍스트를 입력하세요."
        />
        <button onClick={() => encrypt(plainText)}>암호화</button>
      </div>
      <div>
        <p>암호화된 텍스트: {encryptedText}</p>
      </div>
      <div>
        <button onClick={() => decrypt(encryptedText)}>복호화</button>
        <p>복호화된 텍스트: {decryptedText}</p>
      </div>
    </div>
  );
};

export default CryptoStudy;
