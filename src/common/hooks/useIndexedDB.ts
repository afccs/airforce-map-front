const DB_NAME = 'indexedDB';
export const STORE_NAME = 'NolibraryUser';

// IndexedDB를 초기화하는 함수
export const openDB = () => {
    return new Promise<IDBDatabase>((resolve, reject) => {
      const request = indexedDB.open(DB_NAME, 1);
  
      //1.새로운 데이터베이스 생성, 2.기존 데이터베이스보다 높은 버전을 요청할때
      request.onupgradeneeded = (event) => {
        const db = (event.target as IDBOpenDBRequest).result;
        if (!db.objectStoreNames.contains(STORE_NAME)) {
          db.createObjectStore(STORE_NAME, { keyPath: 'id', autoIncrement: true });
        }
      };
      //onupgradeneeded -> onsuccess 실행됨
      request.onsuccess = (event) => {
        resolve((event.target as IDBOpenDBRequest).result);
      };
  
      request.onerror = (event) => {
        reject((event.target as IDBRequest).error);
      };
    });
}
  
// 데이터를 추가하는 함수
export const addItem = (db: IDBDatabase, item: { name: string, age: number }) => {
    return new Promise<IDBValidKey>((resolve, reject) => {
        const transaction = db.transaction(STORE_NAME, 'readwrite');
        const store = transaction.objectStore(STORE_NAME);
        const request = store.add(item);

        request.onsuccess = () => {
            resolve(request.result);
        };

        request.onerror = () => {
            reject(request.error);
        };
    });
}

// 데이터를 읽어오는 함수
export const getItem = (db: IDBDatabase, id: number) => {
    return new Promise<any>((resolve, reject) => {
        const transaction = db.transaction(STORE_NAME, 'readonly');
        const store = transaction.objectStore(STORE_NAME);
        const request = store.get(id);

        request.onsuccess = () => {
            resolve(request.result);
        };

        request.onerror = () => {
            reject(request.error);
        };
    });
}