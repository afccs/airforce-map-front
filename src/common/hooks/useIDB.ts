import { openDB } from 'idb';

const DB_NAME = 'idb';
const STORE_NAME = 'User';

//DB 초기화(없으면 새로 생성)
export const initDB = async () => {
    return openDB(DB_NAME, 1, {
        upgrade(db) {
            if(!db.objectStoreNames.contains(STORE_NAME)){
                db.createObjectStore(STORE_NAME, { keyPath: 'id', autoIncrement: true})
            }
        },
    })
}

export const addUser = async (user: { name: string; age: number }) => {
    const db = await initDB();
    const tx = db.transaction(STORE_NAME, 'readwrite');
    const store = tx.objectStore(STORE_NAME);
    await store.add(user);
    await tx.done;
}

export const getUsers = async () => {
    const db = await initDB();
    const tx = db.transaction(STORE_NAME, 'readonly');
    const store = tx.objectStore(STORE_NAME);
    return await store.getAll();
}

export const updateUser = async (id: number, updateUser: { name: string; age: number }) => {
    const db = await initDB();
    const tx = db.transaction(STORE_NAME, 'readwrite');
    const store = tx.objectStore(STORE_NAME);
    const user = await store.get(id);
    if(user){
        await store.put({ ...user, ...updateUser });
    } 
    await tx.done;
}

export const deleteUser = async (id: number) => {
    const db = await initDB();
    const tx = db.transaction(STORE_NAME, 'readwrite');
    const store = tx.objectStore(STORE_NAME);
    await store.delete(id);
    await tx.done;
}