import { Typography, styled } from "@mui/material";
import { useNavigate } from "react-router-dom";
import ShortAlert from "../../components/alert/ShortAlert";
import DefaultBox from "../../components/box/DefaultBox";
import BaseButton from "../../components/button/BaseButton";

/**
 * 랜딩 페이지 내 Call To Action 모듈
 * @returns {JSX.Element} React Component
 */
const MainHeader = () => {
	const navigate = useNavigate();

	const navigateToLogin = () => {
		navigate("/auth/login");
	};

	return (
		<Root>
			<DefaultBox>
				<TitleWrapper>
					<Typography variant="h1">D2Map React</Typography>
				</TitleWrapper>

				<BaseButton
					title="로그인"
					color="primary"
					size="large"
					onClick={navigateToLogin}
					sx={{ margin: "10% 0px" }}
				/>
				<ShortAlert title="비밀임" severity="error" />
			</DefaultBox>
		</Root>
	);
};

export default MainHeader;

const Root = styled("div")(({ theme }) => ({
	paddingTop: "10%",
	display: "flex",
	flexDirection: "row",
	alignItems: "center",
	justifyContent: "space-around",
	[theme.breakpoints.down("sm")]: {
		display: "flex",
		flexDirection: "column",
	},
}));

const TitleWrapper = styled("div")(() => ({
	margin: "5% 0px",
}));
