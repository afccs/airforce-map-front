const { contextBridge, ipcRenderer } = require('electron')

//preload 스크립트 파일은 node, DOM, electron API 세가지의 context 에 접근이 가능하기 때문에 bridge 역할을 할 수 있다.
//contextBridge 를 통해서 변수, 함수, 객체를 전달할 수 있다.
contextBridge.exposeInMainWorld('versions', {
  node: () => process.versions.node,
  chrome: () => process.versions.chrome,
  electron: () => process.versions.electron
  // we can also expose variables, not just functions
})

const nameOfInterface = "bridge";

contextBridge.exposeInMainWorld(nameOfInterface, {
    listenChannelMessage: (callback) => ipcRenderer.on("channel", (_, data) => callback(data)),
    sendMessage: (data) => ipcRenderer.send("channel", data),
});