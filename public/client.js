var PORT = 5555;
var HOST = '127.0.0.1';

var client = require("dgram").createSocket("udp4");
const message = Buffer.from('UDP 테스트 중');

client.send(message, 0, message.length, PORT, HOST, function(err, bytes){
    if(err) throw err;
    console.log(`UDP message sent to ${HOST} : ${PORT}`);
    client.close();
})